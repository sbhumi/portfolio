# Swetha's Portfolio
### Follow the link below to access the live site

[sbhumi.gitlab.io/portfolio](sbhumi.gitlab.io/portfolio)

### Preview the site in the screenshot below
![Site Preview](preview.png)

##### Template used for project below
[w3schools](https://www.w3schools.com/w3css/tryit.asp?filename=tryw3css_templates_cv&stacked=h)
